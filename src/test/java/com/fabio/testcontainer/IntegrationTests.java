package com.fabio.testcontainer;

import com.fabio.testcontainer.config.ItemServiceContainer;
import com.fabio.testcontainer.config.MongoDbServer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@Testcontainers
@SpringBootTest
@WebAppConfiguration
@AutoConfigureMockMvc
class IntegrationTests {

    @Container
    private GenericContainer<ItemServiceContainer> itemServiceContainer = new ItemServiceContainer();
    @Container
    private GenericContainer<MongoDbServer> mongoDbServer = new MongoDbServer();
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testSimplePutAndGet() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        MvcResult findItem = mockMvc.perform(get("/customer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON_UTF8)
        )
                .andReturn();
        System.out.println("Response Service - " + findItem.getResponse().getContentAsString() + " -");

        MvcResult createUser = mockMvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(CustomerEntity.builder().name("Fábio").age(20L).build())))
                .andReturn();

        CustomerEntity postResponse = objectMapper.readValue(createUser.getResponse().getContentAsString(), CustomerEntity.class);

        System.out.println("Response POST Mongo - " + postResponse);

        MvcResult getUser = mockMvc.perform(get("/customer/" + postResponse.getId())
                .characterEncoding("utf-8")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        CustomerEntity getResponse = objectMapper.readValue(getUser.getResponse().getContentAsString(), CustomerEntity.class);

        System.out.println("Response GET Mongo - " + getResponse);


    }
}