package com.fabio.testcontainer.config;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class ItemServiceContainer extends GenericContainer<ItemServiceContainer> {

    public static final String DEFAULT_IMAGE_AND_TAG = "fabiodarosa/rest-service-test";

    public ItemServiceContainer() {
        super(DEFAULT_IMAGE_AND_TAG);

        withEnv("SERVER_PORT", "9000");
        withEnv("SERVER_NAME", "Sla");
        addExposedPort(9000);
        addFixedExposedPort(9000, 9000);
        setWaitStrategy(Wait.forLogMessage(".*Started NetflixApplication.*", 1));
    }
}

//98