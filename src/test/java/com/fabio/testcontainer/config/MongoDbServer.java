package com.fabio.testcontainer.config;

import org.testcontainers.containers.GenericContainer;

public class MongoDbServer extends GenericContainer<MongoDbServer> {


    public static final String DEFAULT_IMAGE_AND_TAG = "mongo";

    public MongoDbServer() {
        super(DEFAULT_IMAGE_AND_TAG);
        addExposedPort(27017);
        addFixedExposedPort(27017, 27017);
        addEnv("AUTH", "no");
    }
}
