package com.fabio.testcontainer;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/customer")
@AllArgsConstructor
public class Controller {

    private CustomerRepository repository;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/{customerId}")
    public CustomerEntity getCustomer(@PathVariable String customerId) {
        return repository.findById(customerId).orElseThrow();
    }

    @GetMapping
    public String restTest() {
        return restTemplate.exchange("/item", HttpMethod.GET, null, String.class).getBody();

    }


    @PostMapping
    public CustomerEntity createCustomer(@RequestBody CustomerEntity customerEntity) {
        return repository.save(customerEntity);

    }

}
